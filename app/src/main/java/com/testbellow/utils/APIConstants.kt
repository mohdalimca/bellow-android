package com.testbellow.utils

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import com.testbellow.BuildConfig


class APIConstants {
    companion object {

        //Twilio Constants

        const val CALL_SID_KEY: String = "CALL_SID"

        const val VOICE_CHANNEL_LOW_IMPORTANCE = "notification-channel-low-importance"
        const val VOICE_CHANNEL_HIGH_IMPORTANCE = "notification-channel-high-importance"
        const val INCOMING_CALL_INVITE = "INCOMING_CALL_INVITE"
        const val CANCELLED_CALL_INVITE = "CANCELLED_CALL_INVITE"
        const val INCOMING_CALL_NOTIFICATION_ID = "INCOMING_CALL_NOTIFICATION_ID"
        const val ACTION_ACCEPT = "ACTION_ACCEPT"
        const val ACTION_REJECT = "ACTION_REJECT"
        const val ACTION_INCOMING_CALL_NOTIFICATION = "ACTION_INCOMING_CALL_NOTIFICATION"
        const val ACTION_INCOMING_CALL = "ACTION_INCOMING_CALL"
        const val ACTION_CANCEL_CALL = "ACTION_CANCEL_CALL"
        const val ACTION_FCM_TOKEN = "ACTION_FCM_TOKEN"

       // End of Twilio Constants


        var APP_VERSION = "1.0"
        var DEVICE_TYPE = "android"
        var DEVICE_NOTIFICATION_ID = "123"
        var AUTHORIZATION_KEY = ""


        var UPTODATE = 999
        var MAJOR_UPDATE = 1000
        var MINOR_UPDATE = 2000
        var SESSION_EXPIRE = 3000

        var API_STATUS = 0

        var BASE_URL = BuildConfig.SERVER_URL

        //var CHECK_EMAIL_API= BASE_URL +"apiusers/check_email";

        var LOGIN_API = BASE_URL + "user/auth/login"

        var SIGNUP_API = BASE_URL + "user/auth/signup"

        var FORGOT_PWS_API = BASE_URL + "user/auth/forgot_password"


        var RESEND_OTP_API = BASE_URL + "user/auth/resend_email_verification_code\n"

        var RESET_PWS_API = BASE_URL + "rxdelivery/reset_password"

        var OTP_VERIFY_API = BASE_URL + "user/auth/validate_otp"

        var DASHBOARD_API = BASE_URL + "rxdelivery/home_page"

        var LOGOUT_API = BASE_URL + "rxdelivery/logout"

        var ALL_ORDER_API = BASE_URL + "rxdelivery/all_order"

        var VIEW_ORDER_API = BASE_URL + "rxdelivery/view_order"

        var UPDATE_ORDER_API = BASE_URL + "rxdelivery/update_order"

        var CHANGE_PASSOWRD_API = BASE_URL + "/user/auth/change_password"

 var VALIDATE_OTP_API = BASE_URL + "/user/auth/validate_otp"


        @JvmStatic
        fun hardUpdateToPlayStore(context: Context) = run {
            val appPackageName =
                context.packageName // package name of the app

            try {
                context.startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("market://details?id=$appPackageName")
                    )
                )
            } catch (anfe: ActivityNotFoundException) {
                context.startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                    )
                )
            }
        }
    }


}