package com.testbellow.utils.viewmodalfactory

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.testbellow.activity.forgotpassword.ForgotPasswordViewModel
import com.testbellow.activity.home.HomeViewModel
import com.testbellow.activity.login.LoginViewModel
import com.testbellow.activity.otp.OTPViewModel
import com.testbellow.activity.signup.SignUpViewModel
import com.testbellow.activity.splash.SplashViewModel
import com.testbellow.activity.upload_contact.UploadContactViewModel
import com.testbellow.toolbar.ToolBarViewModel
import com.testbellow.utils.server.serverResponseNavigator


class ViewModelProviderFactory( private val application: Application, serverResponse : serverResponseNavigator)

        : ViewModelProvider.AndroidViewModelFactory(application) {


    lateinit var serverResponse : serverResponseNavigator;

    init {
        this.serverResponse = serverResponse;
    }

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SplashViewModel::class.java)) {

            return SplashViewModel(application) as T

        }else if (modelClass.isAssignableFrom(LoginViewModel::class.java)) {

            return LoginViewModel(application, serverResponse) as T

        }else if (modelClass.isAssignableFrom(SignUpViewModel::class.java)) {

            return SignUpViewModel(application, serverResponse) as T

        }else if (modelClass.isAssignableFrom(OTPViewModel::class.java)) {

            return OTPViewModel(application, serverResponse) as T

        }else if (modelClass.isAssignableFrom(ForgotPasswordViewModel::class.java)) {

            return ForgotPasswordViewModel(application, serverResponse) as T

        }else if (modelClass.isAssignableFrom(HomeViewModel::class.java)) {

            return HomeViewModel(application, serverResponse) as T

        }else if (modelClass.isAssignableFrom(ToolBarViewModel::class.java)) {

            return ToolBarViewModel(application) as T
        }else if (modelClass.isAssignableFrom(UploadContactViewModel::class.java)) {

            return UploadContactViewModel(application) as T
        }else if (modelClass.isAssignableFrom(com.testbellow.activity.signup_otp.OTPViewModel::class.java)) {

            return com.testbellow.activity.signup_otp.OTPViewModel(application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class: " + modelClass.name)
    }

}
