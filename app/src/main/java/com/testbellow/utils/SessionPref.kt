package com.testbellow.utils

import android.content.Context
import android.content.SharedPreferences
import android.text.TextUtils
import com.google.gson.Gson
import com.testbellow.activity.login.model.LoginData


class SessionPref(context: Context) {
    val TAG = SessionPref::class.java.simpleName
    val loginPrefKey: String = "loginPrefKey";
    val loginPrefValue: String = "loginPrefValue";


    val cartCountPrefKey: String = "cartCountPrefKey";
    val cartCountPrefValue: String = "cartCountPrefValue";
    val firebaseRegId: String = "firebaseRegId";


     var mLoginData: LoginData ? = null;
    var mContext: Context = context


    init {
       // getSessionPref()
    }

    fun getUserSessionKey(): SharedPreferences {
        var loginSharedPref = mContext!!.getSharedPreferences(loginPrefKey, Context.MODE_PRIVATE);
        return loginSharedPref;
    }



    fun saveSessionPref(userData: LoginData) {
        var loginEditor = getUserSessionKey().edit();
        var gson = Gson();
        var str = gson.toJson(userData)
        loginEditor.putString(loginPrefValue, str)
        loginEditor.commit()
    }

    fun getSessionPref(): LoginData? {
        var gson = Gson();
        var json = getUserSessionKey().getString(loginPrefValue, "")
//        Log.e(TAG , "jsonjson::::  "+json);
        if (json != null && !TextUtils.isEmpty(json)) {
            mLoginData = gson.fromJson(json, LoginData::class.java)
            return mLoginData as LoginData?
        } else {
            return null;
        }

    }
    /*
    * clear all local value on logout
    * */
    fun clearSession() {
        var loginEditor = getUserSessionKey().edit();
        loginEditor.clear()
        loginEditor.commit()

        var cartCountEditor = getCartPref().edit();
        cartCountEditor.clear()
        cartCountEditor.commit()
    }


    /****** manage cart count ******/

    fun getCartPref(): SharedPreferences {
        var cartSharedPref =
            mContext!!.getSharedPreferences(cartCountPrefKey, Context.MODE_PRIVATE);
        return cartSharedPref;
    }


    fun saveCartPrefValue(count: Int) {
        var editor = getCartPref().edit();
        editor.putInt(cartCountPrefValue, count)
        editor.commit()
    }


    fun getCartPrefValue(): Int {
        var count = getCartPref().getInt(cartCountPrefValue, 0)
        return count;
    }

    /*************/

    /****** firebase RegId******/

    fun getFirebaseRegIdPref(): SharedPreferences {
        var cartSharedPref =
            mContext!!.getSharedPreferences(firebaseRegId, Context.MODE_PRIVATE);
        return cartSharedPref;
    }


    fun saveFirebaseRegIdPrefValue(count: String) {
        var editor = getFirebaseRegIdPref().edit();
        editor.putString(firebaseRegId, count)
        editor.commit()
    }


    fun getFirebaseRegIdPrefValue(): String {
        var count = getFirebaseRegIdPref().getString(firebaseRegId, "")
        return count!!;
    }

    /*************/
}