package com.testbellow.utils

public class ValidationUtil
{
    companion object {
        public fun isEmailValid(email: String): Boolean {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
        }

        public fun isMobileNumberValid(mobile: String): Boolean {

            if(mobile.length>9){
                return true
            }else {
                return false
            }

        }


        public fun isOTPValid(otp: String): Boolean {

            if(otp.length>5){
                return true
            }else {
                return false
            }

        }


        fun isValidPassword(password: String): Boolean {
           // return password.matches("(?=.*[A-Z])(?=.*[@#$%])(?=.*[0-9])(?=.*[a-z]).{8,20}".toRegex())
            return password.matches("(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{8,20}".toRegex())

        }
    }

}