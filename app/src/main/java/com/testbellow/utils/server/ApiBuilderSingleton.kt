package com.testbellow.utils.server

public class ApiBuilderSingleton
{


    companion object {
        var mMethodBuilder: ApiInterface ? = null;
        fun getInstance(): ApiInterface? {
            if(mMethodBuilder == null)
            {
                mMethodBuilder = ApiClientBuilder.getClient().create(ApiInterface::class.java);
            }
            return mMethodBuilder
        }
    }


    private fun ApiBuilderSingleton()
    {

    }
}