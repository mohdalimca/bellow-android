package com.testbellow.utils.server

interface serverResponseNavigator {

    fun showLoaderOnRequest(isShowLoader: Boolean);
    fun onResponse(eventType:String , response:String)
    fun onRequestFailed(eventType:String , response:String)
    fun onRequestRetry();
    fun onSessionExpire(message :String)
    fun onMinorUpdate(message :String)
    fun onAppHardUpdate(message :String)
    fun noNetwork()
}