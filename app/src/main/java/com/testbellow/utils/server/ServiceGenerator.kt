package com.testbellow.utils.server

import android.util.Log
import com.testbellow.utils.APIConstants
import com.testbellow.utils.server.ApiClientBuilder.Companion.getSSLFactory
import com.testbellow.utils.server.ApiClientBuilder.Companion.trustAllCerts

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.net.ssl.X509TrustManager


/**
 * Created by hemanth on 12/26/2016.
 */

object ServiceGenerator {

    val HEADER_AUTHENTICATION = "Authorization"
    val HEADER_VERSION = "version"
    val HEADER_USER_ID = "user_id"
    val HEADER_DEVICE_TYPE = "device_type"
    val HEADER_DEVICE_TOKEN = "device_token"
    var API_BASE_URL = APIConstants.BASE_URL
    private var retrofit: Retrofit? = null


    fun <S> createService(serviceClass: Class<S>, token: String, userId: String): S {


        val httpClient = OkHttpClient.Builder()
            //add timeout time if required
            .readTimeout(60, TimeUnit.SECONDS).connectTimeout(60, TimeUnit.SECONDS)
            .hostnameVerifier(ApiClientBuilder.hostnameVerifier)
            .sslSocketFactory(getSSLFactory(), trustAllCerts[0] as X509TrustManager)
        httpClient.addInterceptor { chain ->
            val original = chain.request()
            val requestBuilder = original.newBuilder()
                .addHeader("Content-Type", "application/json; charset=UTF-8")
                .addHeader("Accept", "application/json")
//                .addHeader(HEADER_DEVICE_TOKEN, APIConstants.DEVICE_NOTIFICATION_TOKEN)
//                .addHeader(HEADER_DEVICE_TYPE, APIConstants.DEVICE_TYPE)
                .addHeader(HEADER_VERSION, APIConstants.APP_VERSION)
            val request = requestBuilder.build()
            chain.proceed(request)
        }

        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        httpClient.addInterceptor(logging)  // <-- this is the important line!
        if (retrofit == null) {
            Log.v("API_BASE_URL", "API_BASE_URL: $API_BASE_URL")
            retrofit = Retrofit.Builder().baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(httpClient.build()).build()
        }

        return retrofit!!.create(serviceClass)
    }
}
