package com.testbellow.utils.server

import com.google.gson.JsonObject

/**
 * Created by hemanth on 12/22/2016.
 */

object JsonElementUtil {

    fun getJsonObject(vararg nameValuePair: String): JsonObject? {
        var HashMap: JsonObject? = null

        if (null != nameValuePair && nameValuePair.size % 2 == 0) {

            HashMap = JsonObject()

            var i = 0

            while (i < nameValuePair.size) {
                HashMap.addProperty(nameValuePair[i], nameValuePair[i + 1])
                i += 2
            }

        }

        return HashMap
    }
}
