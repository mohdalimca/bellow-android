package com.testbellow.utils.server

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

public interface ApiInterface
{

    @POST
    public fun postRequest(@Url url: String, @Body body: JsonObject, @Header("user_id") userId: String
                           , @Header("device_id") deviceId: String): Observable<JsonObject>

    @GET("apiusers/country_code")
     fun getcountryList(): Observable<JsonElement>

    @Multipart
    @POST("apiusers/update_profile_image")
    abstract fun updateGroupAPI(@PartMap partMap: HashMap<String, RequestBody>, @Part file: MultipartBody.Part,
                                @Header("user_id") userId: String , @Header("device_id") deviceId: String
    ): Observable<JsonElement>


    @GET("apiusers/get_cart")
    fun getCartListAPI( @Header("user_id" ) userId: String , @Header("device_id") deviceId: String): Observable<JsonElement>

 @GET("apiusers/help_contact")
    fun getHelpContactAPI(): Observable<JsonElement>

 @GET("apiusers/deals_offers")
    fun getDealsOffersAPI(): Observable<JsonElement>



    @GET("rxdelivery/home_page")
    fun getDashBoardAPI(): Observable<JsonElement>

    @GET("rxaushadhi/logout")
    fun getLogoutAPI(): Observable<JsonElement>



}