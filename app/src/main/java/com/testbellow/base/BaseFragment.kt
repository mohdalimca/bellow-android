/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.testbellow.base

import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import android.os.Bundle
import android.util.Log
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.AndroidViewModel

/**
 * Created by amitshekhar on 09/07/17.
 */

abstract class BaseFragment<T : ViewDataBinding, V : AndroidViewModel> : Fragment() {

    private  var viewDataBinding: T? = null
    private var mViewModel: V? = null

    val TAG = BaseFragment::class.java.simpleName
    /**
     * Override for set binding variable
     *
     * @return variable id
     */
//    abstract val bindingVariable: Int
    abstract fun getBindingVariable(): Int

    /**
     * @return layout resource id
     */
//    @get:LayoutRes
//    abstract val layoutId: Int

    @LayoutRes
    abstract fun getLayoutId(): Int

    /**
     * Override for set view model
     *
     * @return view model instance
     */
//    abstract val viewModel: V
    abstract fun getViewModel(): V


//    val isNetworkConnected: Boolean
//        get() = baseActivity != null && baseActivity!!.isNetworkConnected()

    /*override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BaseActivity<*, *>) {
            this.baseActivity = context
            context.onFragmentAttached()
        }
    }*/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        mViewModel = viewModel
        mViewModel = getViewModel()
//        setHasOptionsMenu(false)
    }





    override fun onCreateView(inflater: LayoutInflater,container: ViewGroup?,savedInstanceState: Bundle?): View? {
//        viewDataBinding = DataBindingUtil.inflate(inflater, layoutId, container, false)
        viewDataBinding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false)
        Log.e(TAG ,"viewoncreate::::::  "+(viewDataBinding!=null))
        return viewDataBinding!!.root
    }

    /*override fun onDetach() {
        baseActivity = null
        super.onDetach()
    }*/

    fun getViewDataBinding(): T {
        return viewDataBinding!!
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        viewDataBinding!!.setVariable(bindingVariable, mViewModel)
        this.mViewModel = if (mViewModel == null) getViewModel() else mViewModel
        Log.e(TAG , "(mViewModel!=null) :::: "+(mViewModel!=null))
        Log.e(TAG , "(getBindingVariable()!=null) :::: "+(getBindingVariable()!=null))
        Log.e(TAG , "viewDataBinding!=null::: "+(viewDataBinding!=null))
        viewDataBinding?.setVariable(getBindingVariable(), mViewModel)
//        viewDataBinding!!.lifecycleOwner = this
        viewDataBinding?.executePendingBindings()
    }


    interface Callback {

        fun onFragmentAttached()

        fun onFragmentDetached(tag: String)
    }
}
