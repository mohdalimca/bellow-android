@file:Suppress("DEPRECATION")

package com.testbellow.base

import android.content.Context
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.provider.Settings.Secure
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.AndroidViewModel
import com.testbellow.AppApplication
import com.testbellow.R
import com.testbellow.utils.DialogUtil
import com.testbellow.utils.SessionPref

public abstract class BaseActivity<T : ViewDataBinding, V : AndroidViewModel>
    : AppCompatActivity() {

    private var mViewDataBinding: T? = null
    private var mViewModel: V? = null
    private var TAG = BaseActivity::class.java.simpleName

    public var isFullView = false;

    private var mToolBarBinding: T? = null
    /**
     * Override for set binding variable
     *
     * @return variable id
     */
    abstract fun getBindingVariable(): Int

    /**
     * @return layout resource id
     */
    @LayoutRes
    abstract fun getLayoutId(): Int


    /**
     * Override for set view model
     *
     * @return view model instance
     */
    abstract fun getViewModel(): V

    override fun onCreate(savedInstanceState: Bundle?) {
//        performDependencyInjection()
        super.onCreate(savedInstanceState)
        performDataBinding()

    }

    fun getViewDataBinding(): T {
        return mViewDataBinding!!
    }

    private fun performDataBinding() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && isFullView) {

//            window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);

            window.decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            Log.e(TAG , "setFUkllView : "+isFullView)
        }

        mViewDataBinding = DataBindingUtil.setContentView<T>(this, getLayoutId())
        this.mViewModel = if (mViewModel == null) getViewModel() else mViewModel
        mViewDataBinding!!.setVariable(getBindingVariable(), mViewModel)
        mViewDataBinding!!.executePendingBindings()
    }


    public fun toolBarBinding()
    {
//        mToolBarViewModel = ViewModelProviders.of(this, ViewModelProviderFactory(application , this))
//            .get(ToolBarViewModel::class.java)
//        mBinding.toolBar.viewModel = mToolBarViewModel
    }
    /*
    * get string from edit text
    * */
    fun getEtString(et: EditText): String {
        val str = et.text.toString().trim();
        return str;
    }

    /*
    *
    * */
    fun getAppString(id: Int): String {
        return getString(id)

    }


    /*
    * define static methods , call any where as per need
    * */
    companion object {


        fun getUniqueId():String
        {
         var android_id = Secure.getString(
             AppApplication.getInstanceValue()!!.getContentResolver(),
            Secure.ANDROID_ID);

                Log.d("Android","Android ID : "+android_id);
            return android_id;

        }

        /*
         * get color from colors.xml
         * */
        fun getColor(context: Context, id: Int): Int {
            val version = Build.VERSION.SDK_INT
            return if (version >= 23) {
                ContextCompat.getColor(context, id)
            } else {
                context.resources.getColor(id)
            }
        }

        /*
   * show common messages
   * */
        fun showErrorMessage(context: Context?, message: String, isExit: Boolean,sessionPref: SessionPref?) {

            if (context != null) {
                DialogUtil.okCancelDialog(context,
                    context.resources.getString(R.string.app_name), message,
                    context.resources.getString(R.string.Ok),
                    "", true, false, object : DialogUtil.Companion.selectOkCancelListener {
                        override fun okClick() {
                            if (isExit && sessionPref != null) {
                                sessionPref!!.clearSession()
//                                (context as Activity).startActivity(PreLoginSignupActivity.getIntent(context,true))
                            }

                        }

                        override fun cancelClick() {
                        }


                    })
            }

        }

        /*
     * get color code
     * */
        fun getParseColor(colorcode: String): Int {
            return Color.parseColor(colorcode)
        }



        /*****************
         * Enable/disable views
         */
        //Set enable / disable view  - true means enable else false means disable
        fun setSelection(view: View, visibility: Boolean) {
            view.isEnabled = visibility
            for (i in 0 until (view as ViewGroup).childCount) {
                val child = view.getChildAt(i)
                if (child is ViewGroup) {
                    setSelection(child, visibility)
                } else {
                    child.isEnabled = visibility
                }
            }
        }

        /*
        * local cart count broad cast
        * */




    }







}