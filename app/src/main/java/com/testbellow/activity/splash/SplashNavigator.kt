package com.testbellow.activity.splash

import android.os.Bundle

interface SplashNavigator {

    fun startOtherActivity(bundle: Bundle?, newClass: Class<*>);

}