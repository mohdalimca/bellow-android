package com.testbellow.activity.splash

import android.content.Context
import android.os.Bundle
import android.os.Handler
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.FirebaseApp
import com.google.firebase.iid.FirebaseInstanceId
import com.testbellow.BR
import com.testbellow.R
import com.testbellow.activity.login.LoginActivity
import com.testbellow.base.BaseActivity
import com.testbellow.databinding.ActivitySplashBinding
import com.testbellow.utils.APIConstants
import com.testbellow.utils.SessionPref
import com.testbellow.utils.server.serverResponseNavigator
import com.testbellow.utils.viewmodalfactory.ViewModelProviderFactory


class SplashActivity : BaseActivity<ActivitySplashBinding, SplashViewModel>(),
    SplashNavigator, serverResponseNavigator {

    lateinit var mSplashViewModel: SplashViewModel
    lateinit var mContext: Context
    lateinit var mBinding: ActivitySplashBinding
    lateinit var mSessionPref: SessionPref

    override fun getBindingVariable(): Int {
        return BR.view
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_splash;
    }

    override fun getViewModel(): SplashViewModel {
        mSplashViewModel = ViewModelProviders.of(this, ViewModelProviderFactory(application, this))
            .get(SplashViewModel::class.java)
        return mSplashViewModel
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = getViewDataBinding()
        mSplashViewModel.setNavigator(this)


        ///GET CONTROLS
        getControl()
    }


    /**
     * Initialize all controls
     */
    fun getControl() {
        mContext = this@SplashActivity
        mSessionPref = SessionPref(mContext)

        Handler().postDelayed({
/*

            if (mSessionPref.getSessionPref() != null && mSessionPref.getSessionPref()!!.userStatus == "1") {

                AUTHORIZATION_KEY = mSessionPref.getSessionPref()!!.authorization


                startActivity(HomeActivity.getIntent(mContext))


            } else {

                startActivity(LoginActivity.getIntent(mContext))

            }*/

            startActivity(LoginActivity.getIntent(mContext))

            finish()
        }, 1500)


        FirebaseApp.initializeApp(this);
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    // Log.w(TAG, "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new Instance ID token
                val token = task.result?.token
                APIConstants.DEVICE_NOTIFICATION_ID = token!!


            })

    }


    /**
     * start other activity
     */
    override fun startOtherActivity(bundle: Bundle?, newClass: Class<*>) {
    }



    override fun noNetwork() {
    }

    override fun showLoaderOnRequest(isShowLoader: Boolean) {
    }

    override fun onResponse(eventType: String, response: String) {
    }

    override fun onRequestFailed(eventType: String, response: String) {
    }

    override fun onRequestRetry() {
    }

    override fun onSessionExpire(message: String) {
    }

    override fun onMinorUpdate(message: String) {
    }

    override fun onAppHardUpdate(message: String) {
    }


}
