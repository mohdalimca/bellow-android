package com.testbellow.activity.home

import android.app.Application
import com.testbellow.base.BaseViewModel
import com.testbellow.utils.APIConstants.Companion.DASHBOARD_API
import com.testbellow.utils.APIConstants.Companion.LOGOUT_API
import com.testbellow.utils.server.*

class HomeViewModel(application: Application, serverResponse: serverResponseNavigator) :
    BaseViewModel<HomeNavigator>(application) {

    lateinit var mApplication: Application
    lateinit var serverResponse: serverResponseNavigator
    lateinit var mRxApiCallHelper: RxAPICallHelper
    lateinit var mApiInterface: ApiInterface
    var mUserIdStr = ""


    init {
        mApplication = application
        this.serverResponse = serverResponse
    }


    fun onHealthClick() {

        getNavigator().healthClick()

    }

    fun onSafetyClick() {
        getNavigator().safetyClick()

    }

    fun onSocialClick() {
        getNavigator().socialClick()

    }

    fun onWorkClick() {
        getNavigator().workClick()

    }


    /****** API implementation *******/

    fun callGetDashBoardAPI() {
        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        serverResponse.showLoaderOnRequest(true)
        mApiInterface = ApiBuilderSingleton.getInstance()!!

        val jsonObj = JsonElementUtil.getJsonObject();

        mRxApiCallHelper.call(
            mApiInterface.getDashBoardAPI(),
            DASHBOARD_API, serverResponse
        )
    }

    fun callLogoutAPI() {
        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        serverResponse.showLoaderOnRequest(true)
        mApiInterface = ApiBuilderSingleton.getInstance()!!

        val jsonObj = JsonElementUtil.getJsonObject();

        mRxApiCallHelper.call(
            mApiInterface.getLogoutAPI(),
            LOGOUT_API, serverResponse
        )
    }


}