package com.testbellow.activity.home

interface HomeNavigator {

    fun showMessage(msg:  String,isCancel: Boolean)
    fun healthClick()
    fun safetyClick()
    fun socialClick()
    fun workClick()


}