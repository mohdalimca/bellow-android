package com.testbellow.activity.home

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.AudioManager
import android.media.Ringtone
import android.media.RingtoneManager
import android.os.Build
import android.os.Bundle
import android.os.VibrationEffect
import android.os.Vibrator
import android.view.Gravity
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.lifecycle.ViewModelProviders
import com.testbellow.BR
import com.testbellow.R
import com.testbellow.activity.login.LoginActivity
import com.testbellow.activity.upload_contact.UploadContactActivity
import com.testbellow.activity.voicecall.VoiceActivity
import com.testbellow.base.BaseActivity
import com.testbellow.databinding.ActivityHomeBinding
import com.testbellow.toolbar.ToolBarNavigator
import com.testbellow.toolbar.ToolBarViewModel
import com.testbellow.utils.APIConstants
import com.testbellow.utils.DialogUtil
import com.testbellow.utils.SessionPref
import com.testbellow.utils.server.serverResponseNavigator
import com.testbellow.utils.viewmodalfactory.ViewModelProviderFactory
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.nav_menu_view.*

import org.json.JSONObject

class HomeActivity : BaseActivity<ActivityHomeBinding, HomeViewModel>(),
    HomeNavigator, ToolBarNavigator, serverResponseNavigator, View.OnClickListener {


    lateinit var mContext: Context
    lateinit var mViewModel: HomeViewModel
    lateinit var mSessionPref: SessionPref
    lateinit var mBinding: ActivityHomeBinding
    private var mShowNetworkDialog: Dialog? = null;
    var mToolBarViewModel: ToolBarViewModel? = null;
    var mOkCancelDialog: Dialog? = null;
    private lateinit var vibrator: Vibrator
    private var isOpenDrawer: Boolean = false;
    lateinit var r: Ringtone;

    private var LOGOUT_TYPE: Int = 5555
    private var APP_EXIT: Int = 6666

    companion object {
        fun getIntent(context: Context): Intent {
            var intent = Intent(context, HomeActivity::class.java);

            return intent;
        }

    }

    override fun getBindingVariable(): Int {
        return BR.viewModel;
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_home
    }

    override fun getViewModel(): HomeViewModel {
        mViewModel = ViewModelProviders.of(this, ViewModelProviderFactory(application, this))
            .get(HomeViewModel::class.java)
        return mViewModel
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = getViewDataBinding();
        mViewModel.setNavigator(this)
        mBinding.viewModel = mViewModel
        getControls()
    }


    @TargetApi(Build.VERSION_CODES.M)
    fun getControls() {
        mContext = this@HomeActivity
        mSessionPref = SessionPref(mContext)
        mToolBarViewModel = ViewModelProviders.of(this, ViewModelProviderFactory(application, this))
            .get(ToolBarViewModel::class.java)
        mBinding.toolBar.toolviewModel = mToolBarViewModel
        mToolBarViewModel!!.setToolBarNavigator(this)

        mBinding.toolBar.tvTitle.text = "Home"
        mBinding.toolBar.tvTitle.setTextColor(BaseActivity.getColor(this, R.color.white))
        mBinding.toolBar.ibLeft.setImageResource(R.drawable.ic_menu_black_24dp)
        mBinding.toolBar.ibLeft.visibility = View.VISIBLE
        mBinding.toolBar.ibRight.visibility = View.GONE

        nav_home.setOnClickListener(this)
        nav_upload_contact.setOnClickListener(this)
        nav_notifications.setOnClickListener(this)
        nav_account.setOnClickListener(this)
        nav_signout.setOnClickListener(this)


        // showLoaderOnRequest(true)
        // mViewModel.callGetDashBoardAPI()

        vibrator = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        var notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE)
        r = RingtoneManager.getRingtone(applicationContext, notification)




        manageAnimation()

    }

    override fun onClick(view: View?) {

        isOpenDrawer = false
        mBinding.drawerLayout.closeDrawer(Gravity.LEFT);

        when (view!!.id) {
            R.id.nav_home -> {

            }

            R.id.nav_upload_contact -> {

                startActivity(UploadContactActivity.getIntent(this))

            }


        }
    }

    /*
   * manage left menu navigation animation
   * */
    fun manageAnimation() {

        mBinding.drawerLayout.setScrimColor(Color.TRANSPARENT)
        val actionBarDrawerToggle = object : ActionBarDrawerToggle(
            this, mBinding!!.drawerLayout, R.string.app_name,
            R.string.app_name
        ) {

            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                super.onDrawerSlide(drawerView, slideOffset)
                val slideX = drawerView.getWidth() * slideOffset
                content_frame.translationX = slideX

            }

            override fun onDrawerOpened(drawerView: View) {
//                super.onDrawerOpened(drawerView)
//                drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_OPEN);
//                tool_bar.ib_left.isClickable = true;

            }

            override fun onDrawerClosed(drawerView: View) {
                super.onDrawerClosed(drawerView)
                isOpenDrawer = false

            }
        }

        mBinding.drawerLayout.addDrawerListener(actionBarDrawerToggle)
    }


    /*
* show all common message from one place
* */
    override fun showMessage(msg: String, isCancel: Boolean) {
        DialogUtil.okCancelDialog(mContext!!,
            getAppString(R.string.app_name), msg, getAppString(R.string.Ok),
            "", true, isCancel, object : DialogUtil.Companion.selectOkCancelListener {
                override fun okClick() {

                    if (isCancel) {
                        when (APIConstants.API_STATUS) {
                            APP_EXIT -> {

                                finishAffinity()
                            }
                            else -> {
                                showLoaderOnRequest(true)
                                mViewModel.callLogoutAPI()
                            }
                        }

                    } else {
                        when (APIConstants.API_STATUS) {
                            APIConstants.MAJOR_UPDATE -> {
                                APIConstants.hardUpdateToPlayStore(mContext)
                            }
                            APIConstants.SESSION_EXPIRE -> {

                                mSessionPref.clearSession()
                                startActivity(LoginActivity.getIntent(mContext))
                                finish()
                            }
                            LOGOUT_TYPE -> {
                                mSessionPref.clearSession()
                                startActivity(LoginActivity.getIntent(mContext))
                                finishAffinity()
                            }


                        }


                    }


                }

                override fun cancelClick() {

                }

            });
    }


    override fun showLoaderOnRequest(isShowLoader: Boolean) {
        if (isShowLoader && mShowNetworkDialog == null) {
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else if (isShowLoader && !mShowNetworkDialog!!.isShowing) {
            mShowNetworkDialog = null
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else {
            if (mShowNetworkDialog != null && isShowLoader == false) {
                DialogUtil.hideLoaderDialog(mShowNetworkDialog!!)
                mShowNetworkDialog = null
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onResponse(eventType: String, response: String) {

        showLoaderOnRequest(false)


        APIConstants.API_STATUS = APIConstants.UPTODATE


        if (eventType == APIConstants.DASHBOARD_API) {
            var msg = JSONObject(response).optString("message", "")

            // var dashBoardBean = Gson().fromJson(response, DashBoardBean::class.java)


        } else if (eventType == APIConstants.LOGOUT_API) {
            var msg = JSONObject(response).optString("message", "")

            APIConstants.API_STATUS = LOGOUT_TYPE;

            showMessage(msg, false)


        }
    }

    override fun onRequestFailed(eventType: String, response: String) {
        showLoaderOnRequest(false)
        showMessage(response, false)

    }

    override fun onRequestRetry() {
        showLoaderOnRequest(false)
    }


    override fun onSessionExpire(message: String) {

        APIConstants.API_STATUS = APIConstants.SESSION_EXPIRE

        showLoaderOnRequest(false)

        showMessage(message, false)
    }

    override fun onMinorUpdate(message: String) {

        APIConstants.API_STATUS = APIConstants.MINOR_UPDATE

        showLoaderOnRequest(false)

        showMessage(message, false)

    }

    override fun onAppHardUpdate(message: String) {

        APIConstants.API_STATUS = APIConstants.MAJOR_UPDATE

        showLoaderOnRequest(false)

        showMessage(message, false)

    }

    override fun noNetwork() {
        showLoaderOnRequest(false)
        showErrorMessage(mContext, getString(R.string.No_internet_connection), false, null)
    }


    override fun rightIBClick() {
    }

    @SuppressLint("WrongConstant")
    override fun backClick() {
        if (isOpenDrawer == false) {
            isOpenDrawer = true
            drawer_layout.openDrawer(Gravity.START);
        } else {
            isOpenDrawer = false
//            drawer_layout.openDrawer(Gravity.LEFT);
        }
    }

    override fun onBackPressed() {
        APIConstants.API_STATUS = APP_EXIT
        showMessage(mContext.getString(R.string.are_you_sure_want_to_exit_the_app), true)
    }

    override fun healthClick() {
        // vibratePhone()
        startActivity(Intent(this, VoiceActivity::class.java))

    }

    override fun safetyClick() {

        playRingTone()
    }

    override fun socialClick() {
    }

    override fun workClick() {
    }


    @SuppressLint("NewApi")
    fun playRingTone() {

        var audioManager: AudioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager;
        audioManager.setStreamVolume(
            AudioManager.STREAM_RING,
            audioManager.getStreamMaxVolume(AudioManager.STREAM_RING),
            0
        )
        audioManager.ringerMode = AudioManager.RINGER_MODE_NORMAL



        if (!r.isPlaying) {
            r.play()
        }


        /*   val notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE)
           val mp = MediaPlayer.create(applicationContext, notification)
           mp.start()*/


//        val notification = RingtoneManager.getActualDefaultRingtoneUri(mContext, RingtoneManager.TYPE_RINGTONE)
//        val mp = MediaPlayer.create(applicationContext, notification)
//        mp.start()


//        val player = MediaPlayer.create(mContext, Settings.System.DEFAULT_RINGTONE_URI)
//        player.start()

    }


    private fun vibratePhone() {

        if (Build.VERSION.SDK_INT >= 26) {
            vibrator.vibrate(
                VibrationEffect.createOneShot(
                    2000,
                    VibrationEffect.DEFAULT_AMPLITUDE
                )
            );
        } else {
            vibrator.vibrate(2000)
        }
    }

}
