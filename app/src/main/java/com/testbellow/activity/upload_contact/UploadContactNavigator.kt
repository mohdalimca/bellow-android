package com.testbellow.activity.upload_contact

import android.os.Bundle

interface UploadContactNavigator {

    fun startOtherActivity(bundle: Bundle?, newClass: Class<*>);
    fun showMessage(msg: String)

    fun addHealthClick1()
    fun removeHealthClick1()

    fun addHealthClick2()
    fun removeHealthClick2()

    fun addHealthClick3()
    fun removeHealthClick3()

    fun addHealthClick4()
    fun removeHealthClick4()

    fun addHealthClick5()
    fun removeHealthClick5()

    fun addSocialClick1()
    fun removeSocialClick1()

    fun addSocialClick2()
    fun removeSocialClick2()

    fun addSocialClick3()
    fun removeSocialClick3()

    fun addSocialClick4()
    fun removeSocialClick4()

    fun addSocialClick5()
    fun removeSocialClick5()

    fun addSafetyClick1()
    fun removeSafetyClick1()

    fun addSafetyClick2()
    fun removeSafetyClick2()

    fun addSafetyClick3()
    fun removeSafetyClick3()

    fun addSafetyClick4()
    fun removeSafetyClick4()

    fun addSafetyClick5()
    fun removeSafetyClick5()

    fun addWorkClick1()
    fun removeWorkClick1()

    fun addWorkClick2()
    fun removeWorkClick2()

    fun addWorkClick3()
    fun removeWorkClick3()

    fun addWorkClick4()
    fun removeWorkClick4()

    fun addWorkClick5()
    fun removeWorkClick5()


}