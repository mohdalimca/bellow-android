package com.testbellow.activity.upload_contact

import android.app.Application
import com.testbellow.base.BaseViewModel


public class UploadContactViewModel(application: Application) :
    BaseViewModel<UploadContactNavigator>(application) {
    lateinit var mApplication: Application;

    var health1Str: String = ""
    var health2Str: String = ""
    var health3Str: String = ""
    var health4Str: String = ""
    var health5Str: String = ""

    var safety1Str: String = ""
    var safety2Str: String = ""
    var safety3Str: String = ""
    var safety4Str: String = ""
    var safety5Str: String = ""

    var social1Str: String = ""
    var social2Str: String = ""
    var social3Str: String = ""
    var social4Str: String = ""
    var social5Str: String = ""

    var work1Str: String = ""
    var work2Str: String = ""
    var work3Str: String = ""
    var work4Str: String = ""
    var work5Str: String = ""

    init {
        mApplication = application;
    }


    //Health
    fun onAddHealthClick1() {
        getNavigator().addHealthClick1()
    }

    fun onRemoveHealthClick1() {
        getNavigator().removeHealthClick1()
    }


    fun onAddHealthClick2() {
        getNavigator().addHealthClick2()
    }

    fun onRemoveHealthClick2() {
        getNavigator().removeHealthClick2()
    }


    fun onAddHealthClick3() {
        getNavigator().addHealthClick3()
    }

    fun onRemoveHealthClick3() {
        getNavigator().removeHealthClick3()
    }


    fun onAddHealthClick4() {
        getNavigator().addHealthClick4()
    }

    fun onRemoveHealthClick4() {
        getNavigator().removeHealthClick4()
    }


    fun onAddHealthClick5() {
        getNavigator().addHealthClick5()
    }

    fun onRemoveHealthClick5() {
        getNavigator().removeHealthClick5()
    }


    //Safety
    fun onAddSafetyClick1() {
        getNavigator().addSafetyClick1()
    }

    fun onRemoveSafetyClick1() {
        getNavigator().removeSafetyClick1()
    }

    fun onAddSafetyClick2() {
        getNavigator().addSafetyClick2()
    }

    fun onRemoveSafetyClick2() {
        getNavigator().removeSafetyClick2()
    }

    fun onAddSafetyClick3() {
        getNavigator().addSafetyClick3()
    }

    fun onRemoveSafetyClick3() {
        getNavigator().removeSafetyClick3()
    }

    fun onAddSafetyClick4() {
        getNavigator().addSafetyClick4()
    }

    fun onRemoveSafetyClick4() {
        getNavigator().removeSafetyClick4()
    }

    fun onAddSafetyClick5() {
        getNavigator().addSafetyClick5()
    }

    fun onRemoveSafetyClick5() {
        getNavigator().removeSafetyClick5()
    }


    //Social
    fun onAddSocialClick1() {
        getNavigator().addSocialClick1()
    }

    fun onRemoveSocialClick1() {
        getNavigator().removeSocialClick1()
    }

    fun onAddSocialClick2() {
        getNavigator().addSocialClick2()
    }

    fun onRemoveSocialClick2() {
        getNavigator().removeSocialClick2()
    }

    fun onAddSocialClick3() {
        getNavigator().addSocialClick3()
    }

    fun onRemoveSocialClick3() {
        getNavigator().removeSocialClick3()
    }

    fun onAddSocialClick4() {
        getNavigator().addSocialClick4()
    }

    fun onRemoveSocialClick4() {
        getNavigator().removeSocialClick4()
    }

    fun onAddSocialClick5() {
        getNavigator().addSocialClick5()
    }

    fun onRemoveSocialClick5() {
        getNavigator().removeSocialClick5()
    }


    //Work
    fun onAddWorkClick1() {
        getNavigator().addWorkClick1()
    }

    fun onRemoveWorkClick1() {
        getNavigator().removeWorkClick1()
    }

    fun onAddWorkClick2() {
        getNavigator().addWorkClick2()
    }

    fun onRemoveWorkClick2() {
        getNavigator().removeWorkClick2()
    }

    fun onAddWorkClick3() {
        getNavigator().addWorkClick3()
    }

    fun onRemoveWorkClick3() {
        getNavigator().removeWorkClick3()
    }

    fun onAddWorkClick4() {
        getNavigator().addWorkClick4()
    }

    fun onRemoveWorkClick4() {
        getNavigator().removeWorkClick4()
    }

    fun onAddWorkClick5() {
        getNavigator().addWorkClick5()
    }

    fun onRemoveWorkClick5() {
        getNavigator().removeWorkClick5()
    }


}