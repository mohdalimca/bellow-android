package com.testbellow.activity.upload_contact

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import com.testbellow.BR
import com.testbellow.R
import com.testbellow.base.BaseActivity
import com.testbellow.databinding.ActivityUploadContactBinding
import com.testbellow.toolbar.ToolBarNavigator
import com.testbellow.toolbar.ToolBarViewModel
import com.testbellow.utils.APIConstants
import com.testbellow.utils.DialogUtil
import com.testbellow.utils.SessionPref
import com.testbellow.utils.server.serverResponseNavigator
import com.testbellow.utils.viewmodalfactory.ViewModelProviderFactory
import org.json.JSONObject

class UploadContactActivity : BaseActivity<ActivityUploadContactBinding, UploadContactViewModel>(),
    UploadContactNavigator, ToolBarNavigator, serverResponseNavigator {


    lateinit var mContext: Context
    lateinit var mViewModel: UploadContactViewModel
    lateinit var mSessionPref: SessionPref
    lateinit var mToolBarViewModel: ToolBarViewModel
    lateinit var mBinding: ActivityUploadContactBinding
    private var mShowNetworkDialog: Dialog? = null;
    var mOkCancelDialog: Dialog? = null;

    companion object {
        fun getIntent(context: Context): Intent {
            var intent = Intent(context, UploadContactActivity::class.java);

            return intent;
        }

    }

    override fun getBindingVariable(): Int {
        return BR.viewModel;
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun getLayoutId(): Int {
        val window = getWindow()

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)

// finally change the color
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.semi_transparent_bg))
        return R.layout.activity_upload_contact
    }

    override fun getViewModel(): UploadContactViewModel {
        mViewModel = ViewModelProviders.of(this, ViewModelProviderFactory(application, this))
            .get(UploadContactViewModel::class.java)
        return mViewModel
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = getViewDataBinding();
        mViewModel.setNavigator(this)
        mBinding.viewModel = mViewModel
        getControls()
    }


    fun getControls() {
        mContext = this@UploadContactActivity
        mSessionPref = SessionPref(mContext)
        mToolBarViewModel = ViewModelProviders.of(this, ViewModelProviderFactory(application, this))
            .get(ToolBarViewModel::class.java)
        mBinding.toolBar.toolviewModel = mToolBarViewModel
        mToolBarViewModel.setToolBarNavigator(this)


        mBinding.toolBar.tvTitle.text = ""
    }

    override fun startOtherActivity(bundle: Bundle?, newClass: Class<*>) {

    }


    /*
* show all common message from one place
* */
    override fun showMessage(msg: String) {
        DialogUtil.okCancelDialog(mContext!!,
            getAppString(R.string.app_name), msg, getAppString(R.string.Ok),
            "", true, false, object : DialogUtil.Companion.selectOkCancelListener {
                override fun okClick() {
                    when (APIConstants.API_STATUS) {
                        APIConstants.MAJOR_UPDATE -> {
                            APIConstants.hardUpdateToPlayStore(mContext!!)
                        }

                    }
                }

                override fun cancelClick() {

                }

            });
    }

    override fun addHealthClick1() {

    }

    override fun removeHealthClick1() {

    }

    override fun addHealthClick2() {

    }

    override fun removeHealthClick2() {

    }

    override fun addHealthClick3() {

    }

    override fun removeHealthClick3() {

    }

    override fun addHealthClick4() {

    }

    override fun removeHealthClick4() {

    }

    override fun addHealthClick5() {

    }

    override fun removeHealthClick5() {

    }

    override fun addSocialClick1() {

    }

    override fun removeSocialClick1() {

    }

    override fun addSocialClick2() {

    }

    override fun removeSocialClick2() {

    }

    override fun addSocialClick3() {

    }

    override fun removeSocialClick3() {

    }

    override fun addSocialClick4() {

    }

    override fun removeSocialClick4() {

    }

    override fun addSocialClick5() {

    }

    override fun removeSocialClick5() {

    }

    override fun addSafetyClick1() {

    }

    override fun removeSafetyClick1() {

    }

    override fun addSafetyClick2() {

    }

    override fun removeSafetyClick2() {

    }

    override fun addSafetyClick3() {

    }

    override fun removeSafetyClick3() {

    }

    override fun addSafetyClick4() {

    }

    override fun removeSafetyClick4() {

    }

    override fun addSafetyClick5() {

    }

    override fun removeSafetyClick5() {

    }

    override fun addWorkClick1() {

    }

    override fun removeWorkClick1() {

    }

    override fun addWorkClick2() {

    }

    override fun removeWorkClick2() {

    }

    override fun addWorkClick3() {

    }

    override fun removeWorkClick3() {

    }

    override fun addWorkClick4() {

    }

    override fun removeWorkClick4() {

    }

    override fun addWorkClick5() {

    }

    override fun removeWorkClick5() {

    }


    override fun showLoaderOnRequest(isShowLoader: Boolean) {
        if (isShowLoader && mShowNetworkDialog == null) {
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else if (isShowLoader && !mShowNetworkDialog!!.isShowing) {
            mShowNetworkDialog = null
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else {
            if (mShowNetworkDialog != null && isShowLoader == false) {
                DialogUtil.hideLoaderDialog(mShowNetworkDialog!!)
                mShowNetworkDialog = null
            }
        }
    }

    override fun onResponse(eventType: String, response: String) {}

    override fun onRequestFailed(eventType: String, response: String) {
        showLoaderOnRequest(false)
        showMessage(response)

    }

    override fun onRequestRetry() {
        showLoaderOnRequest(false)
    }

    override fun onMinorUpdate(message: String) {

        APIConstants.API_STATUS = APIConstants.MINOR_UPDATE

        showLoaderOnRequest(false)

        showMessage(message)

    }


    override fun onAppHardUpdate(message: String) {

        APIConstants.API_STATUS = APIConstants.MAJOR_UPDATE

        showLoaderOnRequest(false)

        showMessage(message)

    }


    override fun onSessionExpire(message: String) {

        APIConstants.API_STATUS = APIConstants.SESSION_EXPIRE

        showLoaderOnRequest(false)

        showMessage(message)
    }

    override fun noNetwork() {
        showLoaderOnRequest(false)
        showErrorMessage(mContext, getString(R.string.No_internet_connection), false, null)
    }



    fun handleResponse(response: String) {
        var json = JSONObject(response)
        if (json.optInt("response_code") == 200) {

            DialogUtil.okCancelDialog(mContext!!,
                getAppString(R.string.app_name),
                json.optString("message"),
                getAppString(R.string.Ok),
                "",
                true,
                false,
                object : DialogUtil.Companion.selectOkCancelListener {
                    override fun okClick() {
                      /*  val intent = Intent(mContext, OTPActivity::class.java)
                        intent.putExtra("EMAIL", mViewModel.mobileNoStr)
                        startActivity(intent)*/
                    }

                    override fun cancelClick() {

                    }

                });

        } else {
            showMessage(json.optString("error"))
        }
    }

    override fun backClick() {
        finish()
    }

    override fun rightIBClick() {
    }
}
