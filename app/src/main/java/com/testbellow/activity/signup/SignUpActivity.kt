package com.testbellow.activity.signup

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.WindowManager
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import com.testbellow.BR
import com.testbellow.R
import com.testbellow.activity.otp.OTPActivity
import com.testbellow.base.BaseActivity
import com.testbellow.databinding.ActivitySignUpBinding
import com.testbellow.utils.APIConstants
import com.testbellow.utils.DialogUtil
import com.testbellow.utils.SessionPref
import com.testbellow.utils.server.serverResponseNavigator
import com.testbellow.utils.viewmodalfactory.ViewModelProviderFactory
import org.json.JSONObject

class SignUpActivity : BaseActivity<ActivitySignUpBinding, SignUpViewModel>(),
    SignUpNavigator, serverResponseNavigator {


    lateinit var mContext: Context
    lateinit var mViewModel: SignUpViewModel
    lateinit var mSessionPref: SessionPref
    lateinit var mBinding: ActivitySignUpBinding
    private var mShowNetworkDialog: Dialog? = null;

    companion object {
        fun getIntent(context: Context): Intent {

            return Intent(context, SignUpActivity::class.java)
        }

    }

    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun getLayoutId(): Int {

        val window = getWindow()

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)

// finally change the color
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.semi_transparent_bg))

        return R.layout.activity_sign_up
    }

    override fun getViewModel(): SignUpViewModel {
        mViewModel = ViewModelProviders.of(this, ViewModelProviderFactory(application, this))
            .get(SignUpViewModel::class.java)
        return mViewModel
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = getViewDataBinding();
        mViewModel.setNavigator(this)
        mBinding.viewModel = mViewModel
        getControls()
    }


    fun getControls() {
        mContext = this@SignUpActivity
        mSessionPref = SessionPref(mContext)

    }


    override fun showLoaderOnRequest(isShowLoader: Boolean) {
        if (isShowLoader && mShowNetworkDialog == null) {
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else if (isShowLoader && !mShowNetworkDialog!!.isShowing) {
            mShowNetworkDialog = null
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else {
            if (mShowNetworkDialog != null && isShowLoader == false) {
                DialogUtil.hideLoaderDialog(mShowNetworkDialog!!)
                mShowNetworkDialog = null
            }
        }
    }

    override fun onResponse(eventType: String, response: String) {
        showLoaderOnRequest(false)

        APIConstants.API_STATUS = APIConstants.UPTODATE


        if (eventType == APIConstants.SIGNUP_API) {
            var msg = JSONObject(response).optString("message", "")
            Log.e("JJJJ", "Response: " + response)



            DialogUtil.okCancelDialog(mContext!!,
                getAppString(R.string.app_name), msg, getAppString(R.string.Ok),
                "", true, false, object : DialogUtil.Companion.selectOkCancelListener {
                    override fun okClick() {

                        next()
                    }

                    override fun cancelClick() {

                    }

                });
        }
    }

    override fun onRequestFailed(eventType: String, response: String) {
        showLoaderOnRequest(false)

        Log.e("dfdsf", "HHHH: " + response)
        showMessage(response)

    }


    override fun onRequestRetry() {

        APIConstants.API_STATUS = APIConstants.UPTODATE

        showLoaderOnRequest(false)
    }


    override fun onSessionExpire(message: String) {

        APIConstants.API_STATUS = APIConstants.SESSION_EXPIRE

        showLoaderOnRequest(false)

        showMessage(message)
    }

    override fun onMinorUpdate(message: String) {

        APIConstants.API_STATUS = APIConstants.MINOR_UPDATE

        showLoaderOnRequest(false)

        showMessage(message)

    }


    override fun onAppHardUpdate(message: String) {

        APIConstants.API_STATUS = APIConstants.MAJOR_UPDATE

        showLoaderOnRequest(false)

        showMessage(message)

    }


    override fun noNetwork() {
        showLoaderOnRequest(false)
        //  showErrorMessage(mContext, getString(R.string.No_internet_connection), false, null)
    }


    /*
* show all common message from one place
* */
    override fun showMessage(msg: String) {
        DialogUtil.okCancelDialog(mContext!!,
            getAppString(R.string.app_name), msg, getAppString(R.string.Ok),
            "", true, false, object : DialogUtil.Companion.selectOkCancelListener {
                override fun okClick() {
                    when (APIConstants.API_STATUS) {
                        APIConstants.MAJOR_UPDATE -> {
                            APIConstants.hardUpdateToPlayStore(mContext)
                        }

                    }
                }

                override fun cancelClick() {

                }

            });
    }

    override fun next() {

        val intent = Intent(mContext, OTPActivity::class.java)
        intent.putExtra("EMAIL", mViewModel.emailStr)
        startActivity(intent)
        //finish()

        /*  showLoaderOnRequest(true)

          val jsonObj = JsonElementUtil.getJsonObject("device_type", "1",
              "phone_no", mViewModel.mobileNoStr,
              "email", mViewModel.emailStr,
              "password", mViewModel.passwordStr,
              "name", mViewModel.nameStr,
              "device_id", APIConstants.DEVICE_NOTIFICATION_ID);

          Ion.with(this).load( APIConstants.SIGNUP_API)
              .setJsonObjectBody(jsonObj)
              .asString()
              .setCallback { e: Exception?, response: String ->

                  showLoaderOnRequest(false)
                  if (e == null) {
                      Log.e("response", "response: "+response)
                      handleResponse(response)


                  } else {
                      Log.e("Exception", "Exception: "+e.message)
                  }
              }*/


    }

    override fun checkBoacLick() {

        if (mViewModel.isCheckBoxCheched){
            mBinding.cbTermsAndCondition.isChecked = true
        }else{
            mBinding.cbTermsAndCondition.isChecked = false
        }
    }

    fun handleResponse(response: String) {
        var json = JSONObject(response)
        if (json.optInt("response_code") == 200) {

            DialogUtil.okCancelDialog(mContext!!,
                getAppString(R.string.app_name),
                json.optString("message"),
                getAppString(R.string.Ok),
                "",
                true,
                false,
                object : DialogUtil.Companion.selectOkCancelListener {
                    override fun okClick() {
                        val intent = Intent(mContext, OTPActivity::class.java)
                        intent.putExtra("EMAIL", mViewModel.emailStr)
                        startActivity(intent)
                    }

                    override fun cancelClick() {

                    }

                });

        } else {
            showMessage(json.optString("error"))
        }
    }

}
