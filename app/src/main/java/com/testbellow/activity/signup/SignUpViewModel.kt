package com.testbellow.activity.signup

import android.app.Application
import android.text.TextUtils
import com.testbellow.R
import com.testbellow.base.BaseActivity
import com.testbellow.base.BaseViewModel
import com.testbellow.utils.APIConstants
import com.testbellow.utils.APIConstants.Companion.DASHBOARD_API
import com.testbellow.utils.APIConstants.Companion.LOGOUT_API
import com.testbellow.utils.ValidationUtil
import com.testbellow.utils.server.*

class SignUpViewModel(application: Application, serverResponse: serverResponseNavigator) :
    BaseViewModel<SignUpNavigator>(application) {

    lateinit var mApplication: Application
    lateinit var serverResponse: serverResponseNavigator
    lateinit var mRxApiCallHelper: RxAPICallHelper
    lateinit var mApiInterface: ApiInterface
    var mUserIdStr = ""
    var nameStr:String =""
    var emailStr:String =""
    var mobileNoStr:String =""
    var passwordStr:String =""
    var isCheckBoxCheched:Boolean = false




    init {
        mApplication = application
        this.serverResponse = serverResponse
    }


    fun onCheckBoacLick(){

        if (isCheckBoxCheched){
            isCheckBoxCheched = false
        }else{
            isCheckBoxCheched = true
        }
        getNavigator().checkBoacLick()
    }

    fun onNextClick(){

        if (isValidAll() == ""){
            getNavigator().next()
        }else {
            getNavigator().showMessage(isValidAll())
        }


    }



    /*
   * perform validation
   * */
    private fun isValidAll() : String {
        if (TextUtils.isEmpty(nameStr)) {
            return getStringfromVM(R.string.Please_enter_name)

        }else if (TextUtils.isEmpty(emailStr)) {
            return getStringfromVM(R.string.Please_enter_email_id)

        }else if (!ValidationUtil.isEmailValid(emailStr)) {
            return  getStringfromVM(R.string.Please_enter_valid_email_Id)

        }else if (TextUtils.isEmpty(mobileNoStr)) {
            return getStringfromVM(R.string.Please_enter_mobile_number)

        }else if (!ValidationUtil.isMobileNumberValid(mobileNoStr)) {
            return  getStringfromVM(R.string.Please_enter_valid_mobile_number)

        }else if (TextUtils.isEmpty(passwordStr)) {
            return getStringfromVM(R.string.Please_enter_the_password)

        }else if (passwordStr.length<8) {
            return getStringfromVM(R.string.Password_should_be_min_8_character)

        }
        else if (!ValidationUtil.isValidPassword(passwordStr)) {
            return getStringfromVM(R.string.Please_enter_valid_password)

        } else if (!isCheckBoxCheched) {
            return getStringfromVM(R.string.please_check_terms_condition)

        } else {
            return ""
        }


    }





}