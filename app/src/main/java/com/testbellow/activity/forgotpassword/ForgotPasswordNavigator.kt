package com.testbellow.activity.forgotpassword

interface ForgotPasswordNavigator {

    fun showMessage(msg:  String)
    fun submitClick()

}