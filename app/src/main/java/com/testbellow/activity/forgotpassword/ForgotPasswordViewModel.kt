package com.testbellow.activity.forgotpassword

import android.app.Application
import android.text.TextUtils
import com.testbellow.R
import com.testbellow.base.BaseActivity
import com.testbellow.base.BaseViewModel
import com.testbellow.utils.APIConstants
import com.testbellow.utils.ValidationUtil
import com.testbellow.utils.server.*

class ForgotPasswordViewModel(application: Application, serverResponse: serverResponseNavigator) :
    BaseViewModel<ForgotPasswordNavigator>(application) {

    lateinit var mApplication: Application
    lateinit var serverResponse: serverResponseNavigator
    lateinit var mRxApiCallHelper: RxAPICallHelper
    lateinit var mApiInterface: ApiInterface
    var mUserIdStr = ""

    var mobileNoStr: String = ""

    init {
        mApplication = application
        this.serverResponse = serverResponse
    }


    fun onSubmitClick() {

        if (isValidAll() == "") {
            getNavigator().submitClick()
        } else {
            getNavigator().showMessage(isValidAll())
        }

    }


    /*
   * perform validation
   * */
    fun isValidAll(): String {
        if (TextUtils.isEmpty(mobileNoStr)) {
            return getStringfromVM(R.string.Please_enter_email_id)
        } else if (!ValidationUtil.isEmailValid(mobileNoStr)) {
            return getStringfromVM(R.string.Please_enter_valid_email_Id)
        } else {
            return ""
        }


    }


    /**
     * Call forgot password API
     */
    private fun callForgotPassAPI() {
        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        serverResponse.showLoaderOnRequest(true)
        mApiInterface = ApiBuilderSingleton.getInstance()!!

        val jsonObj = JsonElementUtil.getJsonObject("contact_no", mobileNoStr);

        mRxApiCallHelper.call(
            mApiInterface.postRequest(
                APIConstants.FORGOT_PWS_API,
                jsonObj!!,
                mUserIdStr,
                BaseActivity.getUniqueId()
            ),
            APIConstants.FORGOT_PWS_API, serverResponse
        )
    }


}