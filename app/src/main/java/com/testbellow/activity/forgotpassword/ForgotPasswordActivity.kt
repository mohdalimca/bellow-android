package com.testbellow.activity.forgotpassword

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.WindowManager
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import com.koushikdutta.ion.Ion
import com.testbellow.BR
import com.testbellow.R
import com.testbellow.activity.otp.OTPActivity
import com.testbellow.base.BaseActivity
import com.testbellow.databinding.ActivityForgotPasswordBinding
import com.testbellow.toolbar.ToolBarNavigator
import com.testbellow.toolbar.ToolBarViewModel
import com.testbellow.utils.APIConstants
import com.testbellow.utils.DialogUtil
import com.testbellow.utils.SessionPref
import com.testbellow.utils.server.JsonElementUtil
import com.testbellow.utils.server.serverResponseNavigator
import com.testbellow.utils.viewmodalfactory.ViewModelProviderFactory
import org.json.JSONObject

class ForgotPasswordActivity :
    BaseActivity<ActivityForgotPasswordBinding, ForgotPasswordViewModel>(),
    ForgotPasswordNavigator, ToolBarNavigator, serverResponseNavigator {


    lateinit var mContext: Context
    lateinit var mViewModel: ForgotPasswordViewModel
    lateinit var mSessionPref: SessionPref
    lateinit var mToolBarViewModel: ToolBarViewModel
    lateinit var mBinding: ActivityForgotPasswordBinding
    private var mShowNetworkDialog: Dialog? = null;
    var mOkCancelDialog: Dialog? = null;

    companion object {
        fun getIntent(context: Context): Intent {
            var intent = Intent(context, ForgotPasswordActivity::class.java);

            return intent;
        }

    }

    override fun getBindingVariable(): Int {
        return BR.viewModel;
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun getLayoutId(): Int {
        val window = getWindow()
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)

// finally change the color
        window.setStatusBarColor(ContextCompat.getColor( this, R.color.semi_transparent_bg))
        return R.layout.activity_forgot_password
    }

    override fun getViewModel(): ForgotPasswordViewModel {
        mViewModel = ViewModelProviders.of(this, ViewModelProviderFactory(application, this))
            .get(ForgotPasswordViewModel::class.java)
        return mViewModel
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = getViewDataBinding();
        mViewModel.setNavigator(this)
        mBinding.viewModel = mViewModel
        getControls()
    }


    fun getControls() {
        mContext = this@ForgotPasswordActivity
        mSessionPref = SessionPref(mContext)
        mToolBarViewModel = ViewModelProviders.of(this, ViewModelProviderFactory(application, this))
            .get(ToolBarViewModel::class.java)
        mBinding.toolBar.toolviewModel = mToolBarViewModel
        mToolBarViewModel.setToolBarNavigator(this)


        mBinding.toolBar.tvTitle.text = ""
    }


    /*
* show all common message from one place
* */
    override fun showMessage(msg: String) {
        DialogUtil.okCancelDialog(mContext!!,
            getAppString(R.string.app_name), msg, getAppString(R.string.Ok),
            "", true, false, object : DialogUtil.Companion.selectOkCancelListener {
                override fun okClick() {
                    when (APIConstants.API_STATUS) {
                        APIConstants.MAJOR_UPDATE -> {
                            APIConstants.hardUpdateToPlayStore(mContext!!)
                        }

                    }
                }

                override fun cancelClick() {

                }

            });
    }


    override fun showLoaderOnRequest(isShowLoader: Boolean) {
        if (isShowLoader && mShowNetworkDialog == null) {
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else if (isShowLoader && !mShowNetworkDialog!!.isShowing) {
            mShowNetworkDialog = null
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else {
            if (mShowNetworkDialog != null && isShowLoader == false) {
                DialogUtil.hideLoaderDialog(mShowNetworkDialog!!)
                mShowNetworkDialog = null
            }
        }
    }

    override fun onResponse(eventType: String, response: String) {
        showLoaderOnRequest(false)

        APIConstants.API_STATUS = APIConstants.UPTODATE


        if (eventType == APIConstants.FORGOT_PWS_API) {
            var msg = JSONObject(response).optString("message", "")

            DialogUtil.okCancelDialog(mContext!!,
                getAppString(R.string.app_name), msg, getAppString(R.string.Ok),
                "", true, false, object : DialogUtil.Companion.selectOkCancelListener {
                    override fun okClick() {

                     /*   val bundle = Bundle();
                        bundle.putString("mobileNumber", mViewModel.mobileNoStr)
                        startActivity(OTPActivity.getIntent(mContext, bundle))*/

                        val intent = Intent(mContext, OTPActivity::class.java)
                        intent.putExtra("EMAIL",mViewModel.mobileNoStr)
                        startActivity(intent)

                    }

                    override fun cancelClick() {

                    }

                });
        }
    }

    override fun onRequestFailed(eventType: String, response: String) {
        showLoaderOnRequest(false)
        showMessage(response)

    }

    override fun onRequestRetry() {
        showLoaderOnRequest(false)
    }

    override fun onMinorUpdate(message: String) {

        APIConstants.API_STATUS = APIConstants.MINOR_UPDATE

        showLoaderOnRequest(false)

        showMessage(message)

    }


    override fun onAppHardUpdate(message: String) {

        APIConstants.API_STATUS = APIConstants.MAJOR_UPDATE

        showLoaderOnRequest(false)

        showMessage(message)

    }


    override fun onSessionExpire(message: String) {

        APIConstants.API_STATUS = APIConstants.SESSION_EXPIRE

        showLoaderOnRequest(false)

        showMessage(message)
    }

    override fun noNetwork() {
        showLoaderOnRequest(false)
        showErrorMessage(mContext, getString(R.string.No_internet_connection), false, null)
    }

    override fun submitClick() {
        /*val bundle = Bundle();
        bundle.putString("mobileNumber", mViewModel.mobileNoStr)
        startActivity(OTPActivity.getIntent(mContext, bundle))
*/

        showLoaderOnRequest(true)

        val jsonObj = JsonElementUtil.getJsonObject( "email" , mViewModel.mobileNoStr);

        Log.e("dsfdsf", "jsonObj:  "+jsonObj)

        Ion.with(this).load( APIConstants.FORGOT_PWS_API)
            .setJsonObjectBody(jsonObj)
            .asString()
            .setCallback { e: Exception?, response: String ->

                showLoaderOnRequest(false)
                Log.e("response", "response: "+response)

                if (e == null) {
                    Log.e("response", "response: "+response)
                    handleResponse(response)


                } else {
                    Log.e("Exception", "Exception: "+e.message)
                }
            }

    }
    fun handleResponse(response: String){
        var json = JSONObject(response)
        if (json.optInt("response_code") == 200) {

            DialogUtil.okCancelDialog(mContext!!,
                getAppString(R.string.app_name), json.optString("message"), getAppString(R.string.Ok),
                "", true, false, object : DialogUtil.Companion.selectOkCancelListener {
                    override fun okClick() {
                         val intent = Intent(mContext, OTPActivity::class.java)
                         intent.putExtra("EMAIL", mViewModel.mobileNoStr)
                         startActivity(intent)
                    }

                    override fun cancelClick() {

                    }

                });

        }else{
            showMessage(json.optString("error"))
        }
    }
    override fun backClick() {
        finish()
    }

    override fun rightIBClick() {
    }


}
