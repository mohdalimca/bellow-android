package com.testbellow.activity.login

interface LoginNavigator {

    fun showMessage(msg:  String)
    fun forgotPasswordClick()
    fun loginClick()
    fun createClick()
    fun passwordToggleClick()

}