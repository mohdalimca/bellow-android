package com.testbellow.activity.login.model


import com.google.gson.annotations.SerializedName

data class LoginData(
    @SerializedName("data")
    val `data`: Data,
    @SerializedName("error")
    val error: String,
    @SerializedName("global_error")
    val globalError: String,
    @SerializedName("message")
    val message: String,
    @SerializedName("response_code")
    val responseCode: Int,
    @SerializedName("service_name")
    val serviceName: String
) {
    data class Data(
        @SerializedName("login_data")
        val loginData: LoginData
    ) {
        data class LoginData(
            @SerializedName("sessionkey")
            val sessionkey: String
        )
    }
}