package com.testbellow.activity.login

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.InputType
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.WindowManager
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.testbellow.BR
import com.testbellow.R
import com.testbellow.activity.forgotpassword.ForgotPasswordActivity
import com.testbellow.activity.home.HomeActivity
import com.testbellow.activity.signup.SignUpActivity
import com.testbellow.base.BaseActivity
import com.testbellow.databinding.ActivityLoginBinding
import com.testbellow.utils.APIConstants
import com.testbellow.utils.DialogUtil
import com.testbellow.utils.SessionPref
import com.testbellow.utils.server.serverResponseNavigator
import com.testbellow.utils.viewmodalfactory.ViewModelProviderFactory
import com.koushikdutta.ion.Ion
import com.testbellow.activity.login.model.LoginData
import com.testbellow.utils.server.JsonElementUtil


import org.json.JSONObject

class LoginActivity : BaseActivity<ActivityLoginBinding, LoginViewModel>(),
    LoginNavigator, serverResponseNavigator {


    lateinit var mContext: Context
    lateinit var mViewModel: LoginViewModel
    lateinit var mSessionPref: SessionPref
    lateinit var mBinding: ActivityLoginBinding
    private var mShowNetworkDialog: Dialog? = null;

    companion object {
        fun getIntent(context: Context): Intent {

            return Intent(context, LoginActivity::class.java)
        }

    }

    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun getLayoutId(): Int {

        val window = getWindow()

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)

// finally change the color
        window.setStatusBarColor(ContextCompat.getColor( this,R.color.semi_transparent_bg))

        return R.layout.activity_login
    }

    override fun getViewModel(): LoginViewModel {
        mViewModel = ViewModelProviders.of(this, ViewModelProviderFactory(application, this))
            .get(LoginViewModel::class.java)
        return mViewModel
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = getViewDataBinding();
        mViewModel.setNavigator(this)
        mBinding.viewModel = mViewModel
        getControls()
    }


    fun getControls() {
        mContext = this@LoginActivity
        mSessionPref = SessionPref(mContext)

    }


    /*
* show all common message from one place
* */
    override fun showMessage(msg: String) {
        DialogUtil.okCancelDialog(mContext!!,
            getAppString(R.string.app_name), msg, getAppString(R.string.Ok),
            "", true, false, object : DialogUtil.Companion.selectOkCancelListener {
                override fun okClick() {
                    when (APIConstants.API_STATUS) {
                        APIConstants.MAJOR_UPDATE -> {
                            APIConstants.hardUpdateToPlayStore(mContext)
                        }

                    }
                }

                override fun cancelClick() {

                }

            });
    }


    override fun showLoaderOnRequest(isShowLoader: Boolean) {
        if (isShowLoader && mShowNetworkDialog == null) {
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else if (isShowLoader && !mShowNetworkDialog!!.isShowing) {
            mShowNetworkDialog = null
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else {
            if (mShowNetworkDialog != null && isShowLoader == false) {
                DialogUtil.hideLoaderDialog(mShowNetworkDialog!!)
                mShowNetworkDialog = null
            }
        }
    }

    override fun onResponse(eventType: String, response: String) {
        showLoaderOnRequest(false)

        APIConstants.API_STATUS = APIConstants.UPTODATE


        if (eventType == APIConstants.LOGIN_API) {
            var msg = JSONObject(response).optString("message", "")

            Log.e("dsfds", "TTTTT: "+response)


           // var loginData = Gson().fromJson(response, LoginData::class.java)

           /* mSessionPref.saveSessionPref(loginData.data)
            APIConstants.AUTHORIZATION_KEY = mSessionPref.getSessionPref()!!.authorization

            val intent = Intent(mContext, HomeActivity::class.java)
            startActivity(intent)
            finish()*/

            /*DialogUtil.okCancelDialog(mContext!!,
                getAppString(R.string.app_name), msg, getAppString(R.string.Ok),
                "", true, false, object : DialogUtil.Companion.selectOkCancelListener {
                    override fun okClick() {

                        loginClick()
                    }

                    override fun cancelClick() {

                    }

                });*/
        }
    }

    override fun onRequestFailed(eventType: String, response: String) {
        showLoaderOnRequest(false)
        showMessage(response)

    }


    override fun onRequestRetry() {

        APIConstants.API_STATUS = APIConstants.UPTODATE

        showLoaderOnRequest(false)
    }


    override fun onSessionExpire(message: String) {

        APIConstants.API_STATUS = APIConstants.SESSION_EXPIRE

        showLoaderOnRequest(false)

        showMessage(message)
    }

    override fun onMinorUpdate(message: String) {

        APIConstants.API_STATUS = APIConstants.MINOR_UPDATE

        showLoaderOnRequest(false)

        showMessage(message)

    }


    override fun onAppHardUpdate(message: String) {

        APIConstants.API_STATUS = APIConstants.MAJOR_UPDATE

        showLoaderOnRequest(false)

        showMessage(message)

    }


    override fun noNetwork() {
        showLoaderOnRequest(false)
      //  showErrorMessage(mContext, getString(R.string.No_internet_connection), false, null)
    }


    override fun forgotPasswordClick() {

        startActivity(ForgotPasswordActivity.getIntent(mContext))
    }

    override fun loginClick() {

        /*val intent = Intent(mContext, HomeActivity::class.java)
        startActivity(intent)
        finish()*/

        showLoaderOnRequest(true)

        val jsonObj = JsonElementUtil.getJsonObject("device_type", "1",
            "password", mViewModel.passwordStr,
            "email", mViewModel.mobileNoStr,
            "device_id", APIConstants.DEVICE_NOTIFICATION_ID);
        Ion.with(this).load( APIConstants.LOGIN_API)
            .setJsonObjectBody(jsonObj)
            .asString()
            .setCallback { e: Exception?, response: String ->

                showLoaderOnRequest(false)
                if (e == null) {
                    Log.e("response", "response: "+response)
                    handleResponse(response)


                } else {
                    Log.e("Exception", "Exception: "+e.message)
                }
            }


    }

    override fun createClick() {
        startActivity(SignUpActivity.getIntent(mContext))

    }

    override fun passwordToggleClick() {

        if (mViewModel.isPasswordOn){
            mBinding.ivPasswordToggle.setImageResource(R.mipmap.ic_password_on)
            mBinding.etPassword.transformationMethod  = PasswordTransformationMethod.getInstance()
            mBinding.etPassword.setSelection(mViewModel.passwordStr.length)
        }else{
            mBinding.ivPasswordToggle.setImageResource(R.mipmap.ic_password_off)
            mBinding.etPassword.transformationMethod  = HideReturnsTransformationMethod.getInstance()
            mBinding.etPassword.setSelection(mViewModel.passwordStr.length)
        }
    }

    fun handleResponse(response: String){

        var json = JSONObject(response)
        Log.e("dfdsf", "Response: "+json)

        if (json.optInt("response_code") == 200){

           var mLoginBean = Gson().fromJson(response, LoginData::class.java)
            mSessionPref!!.saveSessionPref(mLoginBean)
            mSessionPref = SessionPref(mContext!!)

            DialogUtil.okCancelDialog(mContext!!,
                getAppString(R.string.app_name), json.optString("message"), getAppString(R.string.Ok),
                "", true, false, object : DialogUtil.Companion.selectOkCancelListener {
                    override fun okClick() {
                        val intent = Intent(mContext, HomeActivity::class.java)
                        startActivity(intent)
                        finish()
                    }

                    override fun cancelClick() {

                    }

                });
        }else{

            showMessage(json.optString("error"))
        }

    }

}
