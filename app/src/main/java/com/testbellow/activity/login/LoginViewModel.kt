package com.testbellow.activity.login

import android.app.Application
import android.text.TextUtils
import com.testbellow.R
import com.testbellow.base.BaseActivity
import com.testbellow.base.BaseViewModel
import com.testbellow.utils.APIConstants
import com.testbellow.utils.ValidationUtil
import com.testbellow.utils.server.*


class LoginViewModel(application: Application, serverResponse: serverResponseNavigator) :
    BaseViewModel<LoginNavigator>(application) {

    var mApplication: Application = application
    var serverResponse: serverResponseNavigator
    lateinit var mRxApiCallHelper: RxAPICallHelper
    lateinit var mApiInterface: ApiInterface
    var mUserIdStr = ""

    var passwordStr: String = ""
    var mobileNoStr: String = ""
    var isPasswordOn = true

    init {
        this.serverResponse = serverResponse
    }


    fun onForgotPasswordClick(){

        getNavigator().forgotPasswordClick()
    }


    fun onLoginClick(){

        if (isValidAll() == ""){

            getNavigator().loginClick()
//            loginAPI()
        }else {
            getNavigator().showMessage(isValidAll())
        }

    }

    fun onCreateClick(){
        getNavigator().createClick()
    }



    fun onPasswordToggleClick(){
        if (isPasswordOn){
            isPasswordOn = false
        }else{
            isPasswordOn = true
        }
        getNavigator().passwordToggleClick()
    }

    /*
   * perform validation
   * */
    private fun isValidAll() : String {
        if (TextUtils.isEmpty(mobileNoStr)) {
            return getStringfromVM(R.string.Please_enter_email_id)
        }else if (!ValidationUtil.isEmailValid(mobileNoStr)) {
            return  getStringfromVM(R.string.Please_enter_valid_email_Id)
        }else if (TextUtils.isEmpty(passwordStr)) {
            return getStringfromVM(R.string.Please_enter_the_password)
        }/*else if (passwordStr.length<8) {
            return getStringfromVM(R.string.Password_should_be_min_8_character)
        }
        else if (!ValidationUtil.isValidPassword(passwordStr)) {
            return getStringfromVM(R.string.Please_enter_valid_password)
        } */else {
            return ""
        }


    }


    /****** API implementation *******/

    private fun loginAPI() {
        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        serverResponse.showLoaderOnRequest(true)
        mApiInterface = ApiBuilderSingleton.getInstance()!!
/*

"{
  ""device_type"": ""3"",
  ""email"": ""ajay@gmail.com"",
  ""password"": ""12345678"",
 ""device_id"" : ""34345frt54tt""
}

"
*/

        val jsonObj = JsonElementUtil.getJsonObject("device_type", "1",
            "password", passwordStr,
            "email", mobileNoStr,
            "device_id", APIConstants.DEVICE_NOTIFICATION_ID);

        mRxApiCallHelper.call(
            mApiInterface.postRequest(
                APIConstants.LOGIN_API,
                jsonObj!!,
                mUserIdStr,
                BaseActivity.getUniqueId()
            ),
            APIConstants.LOGIN_API, serverResponse
        )
    }



}