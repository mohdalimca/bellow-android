package com.testbellow.activity.signup_otp

import android.os.Bundle

interface OTPNavigator {
//    public fun backClick();
    fun startOtherActivity(bundle: Bundle?, newClass: Class<*>);
    fun mainClick();
    fun resendOTPClick()
    fun resetPasswordClick()
    fun showMessage(msg:  String)

}