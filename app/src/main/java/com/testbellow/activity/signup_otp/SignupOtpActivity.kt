package com.testbellow.activity.signup_otp


import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.koushikdutta.ion.Ion
import com.testbellow.BR
import com.testbellow.R
import com.testbellow.activity.home.HomeActivity
import com.testbellow.activity.login.model.LoginData
import com.testbellow.base.BaseActivity
import com.testbellow.databinding.ActivitySignUpBinding
import com.testbellow.databinding.ActivitySignupOtpBinding
import com.testbellow.toolbar.ToolBarNavigator
import com.testbellow.toolbar.ToolBarViewModel
import com.testbellow.utils.APIConstants
import com.testbellow.utils.DialogUtil
import com.testbellow.utils.KeyboardUtil
import com.testbellow.utils.SessionPref
import com.testbellow.utils.server.JsonElementUtil
import com.testbellow.utils.server.serverResponseNavigator
import com.testbellow.utils.viewmodalfactory.ViewModelProviderFactory
import org.json.JSONObject
import java.util.concurrent.TimeUnit

class SignupOtpActivity : BaseActivity<ActivitySignupOtpBinding, OTPViewModel>(), OTPNavigator,
    ToolBarNavigator, serverResponseNavigator {


    var TAG: String = SignupOtpActivity::class.java.simpleName
    var mContext: Context? = null;
    lateinit var mEmailStr: String;
    lateinit var mViewModel: OTPViewModel
    lateinit var mBinding: ActivitySignupOtpBinding;
    var mToolBarViewModel: ToolBarViewModel? = null;
    var mShowNetworkDialog: Dialog? = null;
    var mOkCancelDialog: Dialog? = null;
    var mOtpStr = "";


    var mSessionPref: SessionPref? = null


    var ERROR_TYPE: Int = 100
    var RESEND_PASSWORD_TYPE: Int = 200
    var OTP_VERIFY_TYPE: Int = 300

    var DIALOG_TYPE: Int = 0

    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    @SuppressLint("NewApi")
    override fun getLayoutId(): Int {
        val window = getWindow()

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)

// finally change the color
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.semi_transparent_bg))

        return R.layout.activity_signup_otp;
    }

    override fun getViewModel(): OTPViewModel {
        mViewModel = ViewModelProviders.of(this, ViewModelProviderFactory(application, this))
            .get(OTPViewModel::class.java)

        return mViewModel
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = getViewDataBinding();
        mViewModel.setNavigator(this)
        getIntentData()
        getControls()
        mViewModel.setToolBarNavigator(this)
    }

    /**
     * Get intent data
     */
    private fun getIntentData() {
        if (intent.hasExtra("EMAIL")) {

            mViewModel.emailStr = intent.getStringExtra("EMAIL")
        }
    }


    @RequiresApi(Build.VERSION_CODES.M)
    fun getControls() {
        mContext = this@SignupOtpActivity
        mSessionPref = SessionPref(mContext!!)

        mToolBarViewModel = ViewModelProviders.of(this, ViewModelProviderFactory(application, this))
            .get(ToolBarViewModel::class.java)
        mBinding.toolBar.toolviewModel = mToolBarViewModel
        mToolBarViewModel!!.setToolBarNavigator(this)

        mBinding.toolBar.tvTitle.text = ""
        mBinding.toolBar.toolbarMainLayout.setBackgroundColor(
            ResourcesCompat.getColor(
                resources,
                android.R.color.transparent,
                null
            )
        )
        mBinding.toolBar.ibLeft.setImageResource(R.mipmap.ic_back)


        resendPasswordTextWithClick();

        mBinding.tvOtpValidTime.visibility = View.VISIBLE
        mBinding.llResend.visibility = View.GONE
        showTimer()
    }

    override fun resetPasswordClick() {
        callVerificationOTPAPI()
    }

    fun callVerificationOTPAPI() {
        showLoaderOnRequest(true)
        val jsonObj = JsonElementUtil.getJsonObject(
            "otp", mViewModel.otpStr, "email", mViewModel.emailStr,
            "device_type", "1", "device_id", APIConstants.DEVICE_NOTIFICATION_ID
        );

        Log.e("dsfdsf", "jsonObj:  " + jsonObj)

        Ion.with(this).load(APIConstants.VALIDATE_OTP_API)
            .setJsonObjectBody(jsonObj)
            .asString()
            .setCallback { e: Exception?, response: String ->

                showLoaderOnRequest(false)
                Log.e("response", "response: " + response)

                if (e == null) {
                    Log.e("response", "response: " + response)
                    handleResponse(response)


                } else {
                    Log.e("Exception", "Exception: " + e.message)
                }
            }
    }


    fun handleResponse(response: String) {
        var json = JSONObject(response)
        if (json.optInt("response_code") == 200) {

            var mLoginBean = Gson().fromJson(response, LoginData::class.java)
            mSessionPref!!.saveSessionPref(mLoginBean)
            mSessionPref = SessionPref(mContext!!)

            DialogUtil.okCancelDialog(mContext!!,
                getAppString(R.string.app_name),
                json.optString("message"),
                getAppString(R.string.Ok),
                "",
                true,
                false,
                object : DialogUtil.Companion.selectOkCancelListener {
                    override fun okClick() {
                         val intent = Intent(mContext, HomeActivity::class.java)
                         startActivity(intent)
                    }

                    override fun cancelClick() {

                    }

                });

        } else {
            showMessage(json.optString("error"))
        }
    }


    override fun resendOTPClick() {
        //call resend OTP API
        showLoaderOnRequest(true)

        val jsonObj = JsonElementUtil.getJsonObject("email", mViewModel.emailStr);

        Log.e("sadasd", "JSON: " + jsonObj)
        Ion.with(this).load(APIConstants.RESEND_OTP_API)
            .setJsonObjectBody(jsonObj)
            .asString()
            .setCallback { e: Exception?, response: String ->

                showLoaderOnRequest(false)
                if (e == null) {
                    Log.e("response", "response: " + response)
                    var json = JSONObject(response)
                    if (json.optInt("response_code") == 200) {
                        showMessage(json.optString("message"))
                    } else {
                        showMessage(json.optString("error"))
                    }


                } else {
                    Log.e("Exception", "Exception: " + e.message)
                }
            }

    }


    override fun rightIBClick() {
    }

    override fun backClick() {
        KeyboardUtil.hideSoftKeyboard((mContext as Activity?)!!)
        finish()
    }


    /*
    * start other activties
    * */
    override fun startOtherActivity(bundle: Bundle?, newClass: Class<*>) {


    }

    override fun showLoaderOnRequest(isShowLoader: Boolean) {
        if (isShowLoader && mShowNetworkDialog == null) {
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else if (isShowLoader && !mShowNetworkDialog!!.isShowing()) {
            mShowNetworkDialog = null
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else {
            if (mShowNetworkDialog != null && isShowLoader == false) {
                DialogUtil.hideLoaderDialog(mShowNetworkDialog!!)
                mShowNetworkDialog = null
            }
        }
    }


    /**
     * API response
     * @param eventType :- API type
     * @param response :- API response
     */
    override fun onResponse(eventType: String, response: String) {

        ////API LOADER DISMISS
        showLoaderOnRequest(false)

        APIConstants.API_STATUS = APIConstants.UPTODATE


        if (eventType == APIConstants.RESEND_OTP_API) {

            var jsonObj = JSONObject(response)
            val message = jsonObj.optString("message", "")


            DIALOG_TYPE = RESEND_PASSWORD_TYPE


            showMessage(message)
            if (timer != null) {
                timer!!.cancel()
            }

            mBinding.tvOtpValidTime.visibility = View.VISIBLE
            mBinding.llResend.visibility = View.GONE

            showTimer()

        } else if (eventType == APIConstants.OTP_VERIFY_API) {


            var jsonObj = JSONObject(response)
            val message = jsonObj.optString("message", "")

            DIALOG_TYPE = OTP_VERIFY_TYPE


            showMessage(message)

            stopTimer()
        }
    }

    /*
    * show timer
    * */
    private var timer: CountDownTimer? = null;

    /**
     * Show timer
     */
    private fun showTimer() {
        timer = object : CountDownTimer(1 * 60000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                var diff = millisUntilFinished
                val secondsInMilli: Long = 1000
                val minutesInMilli = secondsInMilli * 60

                var minute = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished).toString()
                if (TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) < 10) {
                    minute = "0".plus(minute)
                }

                var seconds =
                    ((TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished)) % 60).toString()
                if ((TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished)) % 60 < 10) {
                    seconds = "0".plus(seconds)
                }

                mBinding.tvOtpValidTime.text = minute + ":" + seconds + " sec"


            }

            override fun onFinish() {

                mBinding.tvOtpValidTime.visibility = View.GONE
                mBinding.llResend.visibility = View.VISIBLE

                mBinding.tvOtpValidTime.text = getAppString(R.string.OTP_valid_for) + " 00:00"
            }
        }
        timer!!.start()
    }

    override fun onPause() {
        super.onPause()
        //stopTimer()
    }


    override fun onDestroy() {
        super.onDestroy()
        stopTimer()

    }

    fun stopTimer() {
        if (timer != null) {
            timer!!.cancel()
        }
    }

    override fun onRequestFailed(eventType: String, response: String) {

        DIALOG_TYPE = ERROR_TYPE

        showLoaderOnRequest(false)
        showMessage(response)
    }

    override fun onRequestRetry() {
        showLoaderOnRequest(false)
    }


    /*
   * show all common message from one place
   * */
    override fun showMessage(msg: String) {


        mOkCancelDialog = DialogUtil.okCancelDialog(mContext!!,
            getAppString(R.string.app_name), msg, getAppString(R.string.Ok),
            "", true, false, object : DialogUtil.Companion.selectOkCancelListener {
                override fun okClick() {
                    when (DIALOG_TYPE) {
                        OTP_VERIFY_TYPE -> {

                            val intent = Intent(mContext, HomeActivity::class.java)
                            startActivity(intent)
                            finish()
                        }
                        else -> {


                            when (APIConstants.API_STATUS) {
                                APIConstants.MAJOR_UPDATE -> {
                                    APIConstants.hardUpdateToPlayStore(mContext!!)
                                }

                            }

                        }
                    }


                }

                override fun cancelClick() {

                }
            });

    }


    override fun mainClick() {
        KeyboardUtil.hideSoftKeyBoard(mContext!!, mBinding.clMain)
        mBinding.clMain.requestFocus()
    }

    override fun onSessionExpire(message: String) {

        APIConstants.API_STATUS = APIConstants.SESSION_EXPIRE

        showLoaderOnRequest(false)

        showMessage(message)
    }

    override fun onMinorUpdate(message: String) {

        APIConstants.API_STATUS = APIConstants.MINOR_UPDATE

        showLoaderOnRequest(false)

        showMessage(message)

    }


    override fun onAppHardUpdate(message: String) {

        APIConstants.API_STATUS = APIConstants.MAJOR_UPDATE

        showLoaderOnRequest(false)

        showMessage(message)

    }

    override fun noNetwork() {
        showLoaderOnRequest(false)
    }

    private fun resendPasswordTextWithClick() {


        ////SETTING THE SECOND TEXT
        val firstText = SpannableStringBuilder()

        firstText.append(mContext!!.getString(R.string.Did_not_receive_OTP))

        val resendText = SpannableString(mContext!!.getString(R.string.Resend))
        resendText.setSpan(StyleSpan(Typeface.BOLD), 0, resendText.length, 0)
        firstText.append(resendText)

        val resendClick: ClickableSpan = object : ClickableSpan() {
            override fun onClick(textView: View) {

                resendOTPClick()
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = false
            }
        }


        firstText.setSpan(
            resendClick,  // Span to add
            0,  // Start of the span (inclusive)
            firstText.indexOf("?"),  // End of the span (exclusive)
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE // Do not extend the span when text add later
        )

        val foregroundColorFirst =
            ForegroundColorSpan(ResourcesCompat.getColor(resources, R.color.white, null))
        val foregroundColorResend =
            ForegroundColorSpan(ResourcesCompat.getColor(resources, R.color.green, null))

        print("PRINTING==>>>> " + firstText.toString().indexOf("?"))

        firstText.setSpan(
            foregroundColorFirst,
            0,
            firstText.toString().indexOf("?"),
            0
        )
        firstText.setSpan(
            resendClick,
            firstText.toString().indexOf("?") + 1,
            firstText.toString().indexOf("?") + 8,
            0
        )

        firstText.setSpan(
            foregroundColorResend,
            firstText.toString().indexOf("?") + 1,
            firstText.toString().indexOf("?") + 8,
            0
        )

        mBinding.tvResendPws.setText(firstText, TextView.BufferType.SPANNABLE)
        mBinding.tvResendPws.setMovementMethod(LinkMovementMethod.getInstance())
        mBinding.tvResendPws.setHighlightColor(
            ResourcesCompat.getColor(
                resources,
                android.R.color.transparent,
                null
            )
        )
    }


}
