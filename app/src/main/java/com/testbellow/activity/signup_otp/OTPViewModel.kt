package com.testbellow.activity.signup_otp

import android.app.Application
import android.text.TextUtils
import android.util.Log
import com.testbellow.R
import com.testbellow.base.BaseActivity
import com.testbellow.base.BaseViewModel
import com.testbellow.utils.APIConstants
import com.testbellow.utils.ValidationUtil
import com.testbellow.utils.server.*


class OTPViewModel(application: Application, serverResponse: serverResponseNavigator) :
    BaseViewModel<OTPNavigator>(application) {

    lateinit var mApplication: Application;
    lateinit var serverResponse: serverResponseNavigator;
    lateinit var mRxApiCallHelper: RxAPICallHelper;
    lateinit var mApiInterface: ApiInterface;

    var emailStr: String = ""
    var otpStr: String = ""
    var isPasswordOn = true
    var passwordStr: String = ""

    init {
        mApplication = application;
        this.serverResponse = serverResponse;
    }


    fun clMainClick() {
        getNavigator().mainClick()

    }


    fun resendOTPClick() {
        getNavigator().resendOTPClick()
    }

    fun onResetPasswordClick() {
        if (isValidAll() == ""){
            getNavigator().resetPasswordClick()
        }else{
            getNavigator().showMessage(isValidAll())
        }

    }




    /*
* perform validation
* */
    private fun isValidAll() : String {
        if (TextUtils.isEmpty(otpStr)) {
            return getStringfromVM(R.string.Please_enter_otp)
        }else if (!ValidationUtil.isOTPValid(otpStr)) {
            return  getStringfromVM(R.string.Please_enter_valid_otp)
        }else {
            return ""
        }


    }


    /******* API implementation ******/

    fun callOTPVerifyAPI(phoneStr: String, otpStr: String) {


        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        serverResponse.showLoaderOnRequest(true)
        mApiInterface = ApiBuilderSingleton.getInstance()!!
        /* "{
         ""otp"": ""491253"",
         ""email"": ""kuli12345678934@yopmail.com"",
         ""device_type"": ""3"",
         ""device_id"" : ""34345frt54tt""
     }

     "*/

        val jsonObj = JsonElementUtil.getJsonObject(
            "otp", otpStr, "email", emailStr,
            "device_type", "2", "device_id", APIConstants.DEVICE_NOTIFICATION_ID
        );

        Log.e("OTP VERIFY==>> ", jsonObj.toString())

        mRxApiCallHelper.call(
            mApiInterface.postRequest(
                APIConstants.OTP_VERIFY_API,
                jsonObj!!,
                "",
                BaseActivity.getUniqueId()
            ),
            APIConstants.OTP_VERIFY_API, serverResponse
        )

    }


    fun resendOTPAPI(phoneStr: String) {
        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        serverResponse.showLoaderOnRequest(true)
        mApiInterface = ApiBuilderSingleton.getInstance()!!
        val jsonObj = JsonElementUtil.getJsonObject("contact_no", phoneStr);

        Log.e("RESEND PWS ==>> ", jsonObj.toString())

        mRxApiCallHelper.call(
            mApiInterface.postRequest(
                APIConstants.RESEND_OTP_API,
                jsonObj!!,
                "",
                BaseActivity.getUniqueId()
            ),
            APIConstants.RESEND_OTP_API, serverResponse
        )

    }


}