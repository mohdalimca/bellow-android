package com.testbellow

import android.app.Application
import android.content.Context
import android.util.Log

class AppApplication : Application() {


    var mContext: Context? = null;

    override fun onCreate() {
        super.onCreate()
        mContext = this;
        instance = this

    }

    companion object {

        var instance: AppApplication? = null
        @Synchronized
        fun getInstanceValue(): AppApplication? {
            if (instance == null) {
                instance = AppApplication();
            }
            Log.e("" ,"(instance == null) :::::  "+((instance == null)));
            return instance;
        }
    }

//    @Synchronized
//    fun getInstance(): AppApplication {
//        if (instance == null) {
//            instance = AppApplication()
//
//        }
//        return instance
//    }
}
